

import React, { useContext } from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createStackNavigator } from 'react-navigation-stack'
import ProductList from './Components/Product/ProductList';
import ProductAddedList from './Components/Product/ProductAddedList';
import Events from './Components/Product/Events';
import Order from './Components/Product/Orders';
import Cart from './Components/Product/Cart';
import ConfirmOrder from './Components/Product/ConfirmOrder';
import SignIn from './Screen/SignIn'
import VerifyAccount from './Screen/VerifyAccount'
import AuthLoadingScreen from './Screen/AuthLoadingScreen'
import { Icon } from 'native-base'


import { DMConsumerProvider } from './ContextApi';

const SwitchNavigator = () => {
  return (
    <DMConsumerProvider>
      <SwitchNavigators />
    </DMConsumerProvider>
  )
}

const ProductStackNavigator = createStackNavigator({
  Product:ProductList,
  ProductAddedList,
  ConfirmOrder
},{headerMode:"none"})

const OrderStackNavigator = createStackNavigator({
  ProductAddedList,
  ConfirmOrder
},{headerMode:"none"})

const AppTabNavigator = createBottomTabNavigator({
  Home: {
    screen:ProductStackNavigator,
    navigationOptions:{
      tabBarLabel: 'Home',
      tabBarIcon:({tintColor})=>(
        <Icon
          type="AntDesign"
          name="home"
          style={{ fontSize: 18, color: tintColor }}
        />
      ),
      tabBarOptions: { activeTintColor:'#14B0CC', }
    }
  },
  Events: {
    screen:Events,
    navigationOptions:{
      tabBarLabel: 'Events',
      tabBarIcon:({tintColor})=>(
        <Icon
          type="MaterialIcons"
          name="event"
          style={{ fontSize: 18, color: tintColor }}
        />
      ),
      tabBarOptions: { activeTintColor:'#14B0CC', }
    }
  },
  Order: {
    screen:Order,
    navigationOptions:{
      tabBarLabel: 'Order',
      tabBarIcon:({tintColor})=>(
        <Icon
          type="Feather"
          name="shopping-bag"
          style={{ fontSize: 18, color: tintColor }}
        />
      ),
      tabBarOptions: { activeTintColor:'#14B0CC', }
    }
  },
  Cart: {
    screen:OrderStackNavigator,
    navigationOptions:{
      tabBarLabel: 'Cart',
      tabBarIcon:({tintColor})=>(
        <Icon
          type="AntDesign"
          name="shoppingcart"
          style={{ fontSize: 18, color: tintColor }}
        />
      ),
      tabBarOptions: { activeTintColor:'#14B0CC', }
    }
  }
})


const AppStackNavigator = createStackNavigator({
  AppTabNavigator: {
    screen: AppTabNavigator
  }
},{headerMode:"none"})


const AuthStackNavigator = createStackNavigator({
  SignIn: SignIn,
  VerifyAccount: VerifyAccount,
},{headerMode:"none"})

const SwitchNav = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: AuthStackNavigator,
  App: AppStackNavigator
})

const SwitchNavigators = createAppContainer(SwitchNav)

export default SwitchNavigator;
