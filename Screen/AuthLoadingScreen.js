import React, { Component } from 'react'
import { View, Text, ActivityIndicator,AsyncStorage } from 'react-native'



export default class AuthLoadingScreen extends Component {
    constructor(props) {
        super(props);
        this.loadApp()
    }
    
    loadApp = async () => {
        const userToken = await AsyncStorage.getItem('userToken')
        this.props.navigation.navigate(userToken ? 'App' : 'Auth')
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Text>Loading</Text>
                <ActivityIndicator />
            </View>
        );
    }
}

