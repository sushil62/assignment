import React,{useContext} from 'react'
import { View, Text, SafeAreaView, TextInput, Image, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import { DMConsumer } from '../ContextApi'

const SignIn = ({navigation}) => {
    const {handleChange,MNumber} = useContext(DMConsumer)

    const isEnabled = MNumber.length === 10

    return (
        <SafeAreaView style={{ flex: 1, justifyContent: "space-between", backgroundColor: "#212121" }}>
            <View style={{ justifyContent: 'flex-end', flex: 1, bottom: 30, left: 20 }}>
                <Image source={{ uri: "https://static.wixstatic.com/media/d48596_020d038bf0aa4dbaa6fd2b65db9d74a9~mv2_d_2953_2126_s_2.png/v1/fill/w_176,h_125,al_c,q_85,usm_0.66_1.00_0.01/HighRes_Dusminute_Logo_PNG.webp" }} style={{ width: 150, height: 100 }} />
            </View>

            <View style={{ backgroundColor: "#fff", height: "60%", borderTopEndRadius: 20, borderTopLeftRadius: 20, padding: 20, justifyContent: "space-between" }}>
                <View style={{ marginVertical: 20 }}>
                    <Text style={{ fontWeight: "700", fontSize: 24 }}>Sign In to Continue</Text>
                    <Text style={{ color: "#999" }}>Enter mobile number to receive OTP</Text>
                </View>

                <View style={{ marginVertical: 20 }}>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                        <Icon type="AntDesign" name="mobile1" />
                        <TextInput onChangeText={text=>handleChange(text,'MNumber')} placeholder="Enter Your Mobile No" keyboardType="number-pad" style={{ borderBottomWidth: 1, borderBottomColor: "#dcdcdc", width: "90%" }} />
                    </View>
                </View>
                <View>
                    <TouchableOpacity disabled={!isEnabled} onPress={()=>navigation.push('VerifyAccount')} style={{ backgroundColor: isEnabled?"#00D0C0":"#999", height: 36, borderRadius: 36, alignItems: "center", justifyContent: "center", marginVertical: 20 }}>
                        <Text style={{color:"#fff"}}>Sign In</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: "flex-end" }}>
                        <Text style={{color:"#03a9f4"}}>Forget Password!</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: "#00D0C0", height: 36, borderRadius: 36, alignItems: "center", justifyContent: "center", marginVertical: 20 }}>
                        <Text style={{color:"#fff"}}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default SignIn
