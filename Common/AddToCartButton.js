import React from 'react'
import { View, Text,TouchableOpacity } from 'react-native'

const AddToCartButton = ({pressFunc}) => {
    return (
        <TouchableOpacity onPress={pressFunc} style={{height:40,width:150,borderRadius:50,backgroundColor:"#ffca28",paddingVertical:8,paddingHorizontal:20,alignItems:"center",justifyContent:"center",elevation:1}}>
            <Text style={{color:"#fff"}}>Add to Cart</Text>
        </TouchableOpacity>
    )
}

export default AddToCartButton
