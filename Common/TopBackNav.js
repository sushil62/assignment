import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Icon } from 'native-base'
import Constants from 'expo-constants';

const TopBackNav = ({ backFunc, title, extraFunc }) => {
    return (
        <View style={{ flexDirection: "row", backgroundColor: "#00D0C0", paddingTop: Constants.statusBarHeight + 12, paddingBottom: 12, alignItems: "center", paddingHorizontal: 20, justifyContent: "space-between" }}>
            <TouchableOpacity onPress={backFunc}>
                <Icon type="AntDesign" name="left" style={{ fontSize: 24, color: "#fff" }} />
            </TouchableOpacity>
            <Text style={{ color: "#fff", fontWeight: "700" }}>{title}</Text>
            <TouchableOpacity><Icon type="AntDesign" name="search1" style={{ fontSize: 24, color: "#fff" }} /></TouchableOpacity>
        </View>
    )
}

export default TopBackNav
