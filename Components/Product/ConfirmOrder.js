import React,{useContext} from 'react'
import { View, Text, Image, SafeAreaView,TouchableOpacity } from 'react-native'
import Constants from 'expo-constants';
import { DMConsumer } from '../../ContextApi'

const ConfirmOrder = ({navigation}) => {
    const {resetProduct} = useContext(DMConsumer)
    return (
        <SafeAreaView style={{ backgroundColor: "#f9f9f9", flex: 1, paddingTop: Constants.statusBarHeight + 12 }}>
            <View style={{ flex: 1, justifyContent: "space-between" }}>
                <View style={{ alignItems: "center", marginVertical: 20 }}>
                    <Text style={{ fontWeight: "700", fontSize: 30 }}>Thank You</Text>
                    <Text style={{ color: "#9e9e9e" }}>Your Order has been Successfully</Text>
                    <Text style={{ color: "#9e9e9e" }}>Placed</Text>
                </View>
                <Image source={{ uri: "https://cdn.dribbble.com/users/889322/screenshots/7792483/media/92dc9f2741129a8af0133c77e51c9699.gif", height: "80%" }} />
            </View>
            <TouchableOpacity onPress={()=>{navigation.navigate('Product');resetProduct()}} style={{ backgroundColor: "#00D0C0", height: 36, borderRadius: 36, alignItems: "center", justifyContent: "center", marginVertical: 20,marginHorizontal:20 }}>
                        <Text style={{color:"#fff"}}>Place New Order</Text>
                    </TouchableOpacity>
        </SafeAreaView>
    )
}

export default ConfirmOrder
