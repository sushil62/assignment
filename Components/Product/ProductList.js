import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, Image, FlatList ,SafeAreaView} from 'react-native'
import AddToCartButton from '../../Common/AddToCartButton'
import TopBackNav from '../../Common/TopBackNav'
import { DMConsumer } from '../../ContextApi'

const ProductList = ({navigation}) => {
    const { product, productIncDec} = useContext(DMConsumer)
    const orderArr=product.filter((item => item.qNum !=0));
    const oLength = orderArr.length
    return (
        <SafeAreaView style={{backgroundColor:"#fff",flex:1}}>
            <TopBackNav title="Product" />
            <View style={{ marginHorizontal: 20, flex: 1, marginVertical: 20, flex: 1 }}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={product}
                    renderItem={({ item }) => <View key={item.id} style={{ padding: 8, borderWidth: 1, borderColor: "#f5f5f5", borderRadius: 8, marginBottom: 8, flexDirection: "row", alignItems: "center", flex: 1 }}>
                        <Image source={{ uri: item.image }} style={{ width: 80, height: 100 }} />
                        <View style={{ marginLeft: 12, borderLeftWidth: 1, borderLeftColor: "#dcdcdc", paddingLeft: 12, flex: 1 }}>
                            <Text>{item.pName.toUpperCase()}</Text>
                            <Text style={{ fontWeight: "700", marginVertical: 4 }}>{item.brand.toUpperCase()}</Text>
                            <Text style={{ color: "#9e9e9e" }}>{item.quantity}</Text>
                            <View style={{ height: 10 }} />

                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={{ color: "#9e9e9e" }}>Price : ₹{item.price}</Text>
                                {item.qNum === 0 ?
                                    <TouchableOpacity onPress={() => { productIncDec(item.id, 1)}} style={{ backgroundColor: "#00D0C0", paddingHorizontal: 30, borderRadius: 20, height: 36, alignItems: "center", justifyContent: "center" }}>
                                        <Text style={{ color: "#fff" }}>Add</Text>
                                    </TouchableOpacity> :
                                    <View style={{ flexDirection: "row" }}>
                                        <TouchableOpacity onPress={() => productIncDec(item.id, -1)} style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopLeftRadius: 20, borderBottomLeftRadius: 20, backgroundColor: "#fff", borderColor: "#00D0C0", borderWidth: 1 }}>
                                            <Text style={{ fontSize: 24 }}>-</Text>
                                        </TouchableOpacity>
                                        <View style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: "#00D0C0", borderTopColor: "#00D0C0", backgroundColor: "#fff" }}>
                                            <Text>{item.qNum}</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => { productIncDec(item.id, 1) }} style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopRightRadius: 20, borderBottomRightRadius: 20, backgroundColor: "#fff", borderColor: "#00D0C0", borderWidth: 1 }}>
                                            <Text>+</Text>
                                        </TouchableOpacity>
                                    </View>}
                            </View>
                        </View>
                    </View>}
                    keyExtractor={item => item.id}
                />
            </View>
            {oLength == 0?null:
            <View style={{ position: "absolute", bottom: 90, right: 20 }}>
                <AddToCartButton pressFunc={()=>navigation.navigate('ProductAddedList')}  />
            </View>}
        </SafeAreaView>
    )
}

export default ProductList
