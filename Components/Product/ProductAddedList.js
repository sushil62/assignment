import { Icon } from 'native-base'
import React, { useContext } from 'react'
import { View, Text, TouchableOpacity, Image, FlatList,SafeAreaView } from 'react-native'
import TopBackNav from '../../Common/TopBackNav'
import { DMConsumer } from '../../ContextApi'



const ProductAddedList = ({navigation}) => {
    const { product, productIncDec } = useContext(DMConsumer)
    const orderArr = product.filter((item => item.qNum != 0));
    const total = orderArr.reduce((sum, li) => sum + li.price * li.qNum, 0)
    const itemLength = orderArr.length === 0
    return (
        <SafeAreaView style={{backgroundColor:"#fff",flex:1}}>
        <TopBackNav title="Selected Item" backFunc={()=>navigation.goBack()} />
        {itemLength?<View style={{flex:1,justifyContent:"center",alignItems:'center'}}><Text>No Item . please Add Item</Text></View>:
        <View style={{ flex: 1, justifyContent: "space-between", marginHorizontal: 20, marginVertical: 20 }}>

            <FlatList
                showsVerticalScrollIndicator={false}
                data={orderArr}
                renderItem={({ item }) => <View key={item.id} style={{ padding: 8, borderWidth: 1, borderColor: "#f5f5f5", borderRadius: 8, marginBottom: 8, flexDirection: "row", alignItems: "center", flex: 1 }}>
                    <Image source={{ uri: item.image }} style={{ width: 80, height: 100 }} />
                    <View style={{ marginLeft: 12, borderLeftWidth: 1, borderLeftColor: "#dcdcdc", paddingLeft: 12, flex: 1 }}>
                        <Text>{item.pName.toUpperCase()}</Text>
                        <Text style={{ fontWeight: "700", marginVertical: 4 }}>{item.brand.toUpperCase()}</Text>
                        <Text style={{ color: "#9e9e9e" }}>{item.quantity}</Text>
                        <View style={{ height: 10 }} />

                        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                            <Text style={{ color: "#9e9e9e" }}>Price : ₹{item.price}</Text>
                            <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity onPress={() => productIncDec(item.id, -1)} style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopLeftRadius: 20, borderBottomLeftRadius: 20, backgroundColor: "#fff", borderColor: "#00D0C0", borderWidth: 1 }}>
                                    <Text style={{ fontSize: 24 }}>-</Text>
                                </TouchableOpacity>
                                <View style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopWidth: 1, borderBottomWidth: 1, borderBottomColor: "#00D0C0", borderTopColor: "#00D0C0", backgroundColor: "#fff" }}>
                                    <Text>{item.qNum}</Text>
                                </View>
                                <TouchableOpacity onPress={() => { productIncDec(item.id, 1) }} style={{ width: 36, height: 36, alignItems: "center", justifyContent: "center", borderTopRightRadius: 20, borderBottomRightRadius: 20, backgroundColor: "#fff", borderColor: "#00D0C0", borderWidth: 1 }}>
                                    <Text>+</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>}
                keyExtractor={item => item.id}
            />
            <View style={{ flexDirection: "row",marginTop:20 }}>
                <View style={{ width: 70, height: 70, backgroundColor: "#f5f5f5", borderRadius: 8, marginRight: 12, alignItems: "center", justifyContent: "center" }}>
                    <Icon type="MaterialCommunityIcons" name="truck-fast" style={{ color: "#424242" }} />
                    {total > 500?<Text  style={{ color: "#424242" }}>Free</Text>:null}
                </View>
                <View style={{ borderBottomWidth: 1, flex: 1, borderBottomColor: "#f5f5f5" }}>
                    <Text style={{ color: "#9e9e9e" }}>Total:</Text>
                    <Text style={{ fontSize: 36, fontWeight: "700" }}>₹ {total}</Text>
                </View>
            </View>
            <TouchableOpacity onPress={()=>navigation.push('ConfirmOrder')} style={{ backgroundColor: "#00D0C0", paddingHorizontal: 30, borderRadius: 40, height: 46, alignItems: "center", justifyContent: "space-between",marginTop:40,flexDirection:"row"}}>
                <Icon type="AntDesign" name="right" style={{color:"#fff",fontSize:20}}/>
                <Text style={{ color: "#fff" }}>Checkout</Text>
                <Text/>
            </TouchableOpacity>
        </View>}
        </SafeAreaView>
    )
}

export default ProductAddedList
