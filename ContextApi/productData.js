export const product = [
    {
        id:"1",
        pName:"Atta - Whole Wheat 1 Kg Pouch",
        brand:"Aashirvaad",
        price:57,
        quantity:"1 PC",
        image:"https://www.aashirvaad.com/assets/Aashirvaad/images/packet-2-right.png",
        qNum:0
    },
    {
        id:"2",
        pName:"Maida",
        brand:"Bhagyalakshmi",
        price:24,
        quantity:"500 G",
        image:"https://img2.pngio.com/wheat-atta-for-the-best-rotis-aashirvaad-atta-aashirvaad-png-227_289.png",
        qNum:0
    },
    {
        id:"3",
        pName:"Gram Flour",
        brand:"Bhagyalakshmi",
        price:48,
        quantity:"500 G",
        image:"https://5.imimg.com/data5/KT/SE/MY-7778582/aashirvaad-atta-500x500.jpg",
        qNum:0
    },
    {
        id:"4",
        pName:"Rice Flour",
        brand:"Bhagyalakshmi",
        price:24,
        quantity:"500 G",
        image:"https://p.kindpng.com/picc/s/381-3816121_rice-bag-png-basmati-rice-bag-png-transparent.png",
        qNum:0
    },
    {
        id:"5",
        pName:"Atta - Whole Wheat",
        brand:"Aashirvaad",
        price:57,
        quantity:"1 PC",
        image:"https://cpimg.tistatic.com/04982077/b/5/Basmati-Rice-Bags.png",
        qNum:0
    },
    {
        id:"6",
        pName:"Rice Flour",
        brand:"Bhagyalakshmi",
        price:24,
        quantity:"500 G",
        image:"https://p.kindpng.com/picc/s/381-3816121_rice-bag-png-basmati-rice-bag-png-transparent.png",
        qNum:0
    },
    {
        id:"7",
        pName:"Atta - Whole Wheat",
        brand:"Aashirvaad",
        price:57,
        quantity:"1 PC",
        image:"https://cpimg.tistatic.com/04982077/b/5/Basmati-Rice-Bags.png",
        qNum:0
    }
]