import React, { Component,createContext } from 'react'
import {product} from './productData'
import AsyncStorage from '@react-native-async-storage/async-storage';

export const DMConsumer = createContext()
export class DMConsumerProvider extends Component {
    state={
        product:product,
        MNumber:'',
        VerifyNumber:'',
        loginToken:''
    }
    handleChange = (text, stateProp) => {
        this.setState({
            [stateProp]: text,
        })
    }
    productIncDec = (id,value) => {
        const productUpdate = this.state.product.map(prd => {
            if (prd.id === id) {
                return {
                    ...prd,
                    qNum: prd.qNum + value,
                    
                };
            } else {
                return prd;
            }
        });

        this.setState({
            product: productUpdate,
        });
    }

    resetProduct=()=>{
        this.setState({
            product:product
        })
    }

    
    signIn = async () => {
        const userToken = await AsyncStorage.getItem('userToken')
        this.setState({
            loginToken:userToken
        })
    }
    componentDidMount(){
        this.signIn()
    }
    
    render() {
        return (
            <DMConsumer.Provider value={{
                ...this.state,
                productIncDec:this.productIncDec,
                handleChange:this.handleChange,
                resetProduct:this.resetProduct
            }}>
                {this.props.children}
            </DMConsumer.Provider>
        )
    }
}
